
data RomeChar = I
              | V
              | X
              | L
              | C
              | D
              | M
              | O
              deriving (Show, Read)

instance Enum RomeChar where
  fromEnum I = 1
  fromEnum V = 5
  fromEnum X = 10
  fromEnum L = 50
  fromEnum C = 100
  fromEnum D = 500
  fromEnum M = 1000
  fromEnum _ = 0
  toEnum 1         = I
  toEnum 5         = V
  toEnum 10        = X
  toEnum 50        = L
  toEnum 100       = C
  toEnum 500       = D
  toEnum 1000      = M
  toEnum _         = O

allChars :: [RomeChar]
allChars = [ I
           , V
           , X
           , L
           , C
           , D
           , M ]

instance Eq RomeChar where
  i == i' = fromEnum i == fromEnum i'

instance Ord RomeChar where
  i <= i' = fromEnum i <= fromEnum i'

newtype RomeNum = MakeRomeNum [RomeChar]
  deriving (Show, Eq)

instance Read RomeNum where
  readsPrec n s = let
                    romeCharFromChar :: Char -> RomeChar
                    romeCharFromChar c = read [c]
                  in [(MakeRomeNum (map romeCharFromChar s), "")]

convertRomeToInteger   :: RomeNum -> Int
convertRomeToInteger (MakeRomeNum r) = sum $ map fromEnum r

convertIntegerToRome   :: Int -> RomeNum
convertIntegerToRome i = MakeRomeNum $ convertIntegerToRome' i (reverse allChars)
  where convertIntegerToRome' 0 _ = []
        convertIntegerToRome' z (h:hs) = divReplicate z h ++ convertIntegerToRome' (z `mod` (fromEnum h)) hs
          where divReplicate x c = replicate (x `div` (fromEnum c)) c

romeAdd     :: RomeNum -> RomeNum -> RomeNum
romeAdd a b = let integerA = convertRomeToInteger a
                  integerB = convertRomeToInteger b
              in convertIntegerToRome $ integerA + integerB

normalizeRomeNum   :: RomeNum -> RomeNum
normalizeRomeNum r = convertIntegerToRome $ convertRomeToInteger $ r

unFoldRomeNum   :: RomeNum -> RomeNum
unFoldRomeNum (MakeRomeNum []) = MakeRomeNum []
unFoldRomeNum (MakeRomeNum (f:[])) = MakeRomeNum [f]
unFoldRomeNum (MakeRomeNum (f:s:ls)) = MakeRomeNum ( unfoldRomeCharPair f s
                                                     ++ listFromRomeNum (unFoldRomeNum $ MakeRomeNum ls) )
  where unfoldRomeCharPair x y | x < y     = listFromRomeNum $ convertIntegerToRome $ fromEnum y - fromEnum x
                               | otherwise = [x, y]

listFromRomeNum                 :: RomeNum -> [RomeChar]
listFromRomeNum (MakeRomeNum l) = l

-- foldRomeNum

romeNumExample1 :: RomeNum
romeNumExample1 = read "VIII"

romeNumExample2 :: RomeNum
romeNumExample2 = read "XIX"

romeNumExample3 :: RomeNum
romeNumExample3 = read "XXIII"

romeChar :: RomeChar
romeChar = read "X"

main1 :: IO ()
main1 = print $ "Rome Numbers test: "
        ++ show romeNumExample1
        ++ " == "
        ++ show romeNumExample2
        ++ " : "
        ++ show (romeNumExample1 == romeNumExample2)

main2 :: IO ()
main2 = do
  print $ "Rome Numbers sum ("
    ++ show romeNumExample1
    ++ ", "
    ++ show romeNumExample3
    ++ ") : "
  print (romeAdd romeNumExample1 romeNumExample3)

main :: IO ()
main = main2
